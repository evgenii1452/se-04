package ru.kozlov.tm.Controller;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Service.ProjectService;
import ru.kozlov.tm.Service.TaskService;

import java.text.ParseException;
import java.util.Map;
import java.util.Scanner;

public class ProjectController extends Controller {

    private TaskService taskService;
    private ProjectService projectService;
    private Scanner scanner = new Scanner(System.in);

    public ProjectController(
            ProjectService projectService,
            TaskService taskService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    public void getAll() {
        System.out.println("[ВАШИ ПРОЕКТЫ]");
        System.out.println(projectService.getAllProjectsAsString());

        System.out.println("[OK]");
    }

    public void create() {
        System.out.println("Введите название проекта:");
        String name = scanner.nextLine();

        System.out.println("Введите описание проекта:");
        String description = scanner.nextLine();

        System.out.println("Введите дату начала проекта в формате \"30/12/1900\"");
        String dateStart = scanner.nextLine();

        System.out.println("Введите дату завершения проекта в формате \"30/12/1900\"");
        String dateEnd = scanner.nextLine();

        try {
            String projectId = projectService.createProject(name, description, dateStart, dateEnd);
            System.out.println("[ПРОЕКТ " + projectId + " СОЗДАН]");
        } catch (IllegalArgumentException | NullPointerException | ParseException e) {
            System.out.println(e.getMessage());
        }

    }

    public void update() {
        System.out.println("[ВАШИ ПРОЕКТЫ]");
        System.out.println(projectService.getAllProjectsAsString());

        System.out.println("Чтобы пропусть поле нажмите Enter");
        System.out.println("Введите id проекта:");
        String projectId = scanner.nextLine();

        System.out.println("Введите новое название проекта:");
        String name = scanner.nextLine();

        System.out.println("Введите новое описание проекта:");
        String description = scanner.nextLine();

        System.out.println("Введите новую дату начала проекта в формате \"30/12/1900\"");
        String dateStart = scanner.nextLine();

        System.out.println("Введите новую дату завершения проекта в формате \"30/12/1900\"");
        String dateEnd = scanner.nextLine();

        try {
            projectService.updateProject(projectId, name, description, dateStart, dateEnd);
            System.out.println("[ПРОЕКТ " + projectId + " ИЗМЕНЕН]");
        } catch (IllegalArgumentException | NullPointerException | ParseException e) {
            System.out.println(e.getMessage());
        }

    }

    public void remove() {
        System.out.println("[ВАШИ ПРОЕКТЫ]");
        System.out.println(projectService.getAllProjectsAsString());

        System.out.println("Введите id проекта:");
        String id = scanner.nextLine();

        try {
            projectService.removeProjectById(id);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());

            return;
        }

        taskService.removeTasksByProjectId(id);

        System.out.println("[ПРОЕКТ #" + id + " УДАЛЕН]");
    }

    public void removeAll() {
        projectService.removeAllProjects();

        System.out.println("[ВСЕ ПРОЕКТЫ УДАЛЕНЫ]");
    }

    public void generate() {
        System.out.println("Введите кол-во проектов для генерации:");
        int count = Integer.parseInt(scanner.nextLine());

        for (int i = 1; i <= count; i++) {
            String name = "Проект #" + i;
            String description = "Описание проекта #" + i;
            String dateStart = "30/12/1900";
            String dateEnd = "30/12/1900";

            try {
                projectService.createProject(name, description, dateStart, dateEnd);
            } catch (IllegalArgumentException | NullPointerException | ParseException e) {
                System.out.println(e.getMessage());
            }
        }

        System.out.println("[ПРОЕКТЫ СГЕНЕРИРОВАНЫ]");
    }
}
