package ru.kozlov.tm.Controller;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Entity.Task;
import ru.kozlov.tm.Service.ProjectService;
import ru.kozlov.tm.Service.TaskService;

import java.text.ParseException;
import java.util.Map;
import java.util.Scanner;

public class TaskController extends Controller{

    private Scanner scanner = new Scanner(System.in);
    private TaskService taskService;
    private ProjectService projectService;

    public TaskController(
            TaskService taskService,
            ProjectService projectService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    public void create() {
        Map<String, Project> projects = projectService.getAllProjects();
        System.out.println("[ВАШИ ПРОЕКТЫ]");

        for (Map.Entry<String, Project> projectEntry: projects.entrySet()) {
            System.out.println(projectEntry.getValue());
        }

        System.out.println("Введите id проекта:");
        String projectId = scanner.nextLine();

        System.out.println("Введите название задачи:");
        String taskName = scanner.nextLine();

        System.out.println("Введите описание задачи:");
        String taskDescription = scanner.nextLine();

        System.out.println("Введите дату начала задачи в формате \"30/12/1900\"");
        String dateStart = scanner.nextLine();

        System.out.println("Введите дату завершения задачи в формате \"30/12/1900\"");
        String dateEnd = scanner.nextLine();

        try {
            String taskId = taskService.createTask(projectId, taskName, taskDescription, dateStart, dateEnd);
            System.out.println("[ЗАДАЧА \"" + taskId + "\" СОЗДАНА]");
        } catch (NullPointerException | IllegalArgumentException | ParseException e) {
            System.out.println(e.getMessage());
        }

    }

    public void update() {
        System.out.println("Чтобы пропусть поле нажмите Enter");

        System.out.println("Введите id задачи:");
        String taskId = scanner.nextLine();

        System.out.println("Введите новое задачи:");
        String name = scanner.nextLine();

        System.out.println("Введите новое описание задачи:");
        String description = scanner.nextLine();

        System.out.println("Введите новую дату начала задачи в формате \"30/12/1900\"");
        String dateStart = scanner.nextLine();

        System.out.println("Введите новую дату завершения задачи в формате \"30/12/1900\"");
        String dateEnd = scanner.nextLine();

        try {
            taskService.updateTask(taskId, name, description, dateStart, dateEnd);
            System.out.println("[ЗАДАЧА " + taskId + " ИЗМЕНЕНА]");
        } catch (IllegalArgumentException | NullPointerException | ParseException e) {
            System.out.println(e.getMessage());
        }

    }

    public void getAll() {
        System.out.println("[ВАШИ ПРОЕКТЫ]");
        System.out.println(projectService.getAllProjectsAsString());

        System.out.println("Введите id проекта:");
        String  projectId = scanner.nextLine();

        try {
            Project project = projectService.getProjectById(projectId);

            Map<String, Task> tasks = taskService.getTasksByProjectId(project.getId());

            System.out.println("Кол-во задач:" + tasks.size());

            tasks.forEach((integer, task) -> System.out.println(task));
        } catch (IllegalArgumentException | NullPointerException e) {
            System.out.println(e.getMessage());
        }
    }

    public void remove() {
        System.out.println("Введите id задачи:");
        String id = scanner.nextLine();

        try {
            taskService.removeTaskById(id);
        } catch (IllegalArgumentException | NullPointerException e) {
            System.out.println(e.getMessage());

            return;
        }

        System.out.println("[ЗАДАЧА #" + id + " УДАЛЕНА]");
    }

    public void removeAll() {
        System.out.println("Введите id проекта:");
        String projectId = scanner.nextLine();

        try {
            projectService.getProjectById(projectId);
        } catch (IllegalArgumentException | NullPointerException e) {
            System.out.println(e.getMessage());

            return;
        }

        taskService.removeTasksByProjectId(projectId);


        System.out.println("[ЗАДАЧИ ПРОЕКТА #" + projectId + " УДАЛЕНЫ]");
    }

    public void generate() {
        Map<String, Project> projects = projectService.getAllProjects();

        for (Map.Entry<String, Project> projectEntry : projects.entrySet()) {
            int count = 1 + (int) (Math.random() * 8);
            String projectId = projectEntry.getKey();

            for (int i = 1; i <= count; i++) {
                String taskName = "Задача #" + i;
                String taskDescription = "Описание задачи #" + i;
                String dateStart = "30/12/1900";
                String dateEnd = "30/12/1900";

                try {
                    taskService.createTask(projectId, taskName, taskDescription, dateStart, dateEnd);
                } catch (NullPointerException | IllegalArgumentException | ParseException e) {
                    System.out.println(e.getMessage());
                }
            }
        }

        System.out.println("[ЗАДАЧИ СГЕНЕРИРОВАНЫ]");
    }

}
