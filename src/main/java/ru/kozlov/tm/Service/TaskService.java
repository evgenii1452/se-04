package ru.kozlov.tm.Service;

import ru.kozlov.tm.Entity.Task;
import ru.kozlov.tm.Repository.TaskRepository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public String createTask(
            String projectId,
            String taskName,
            String taskDescription,
            String dateStartAsString,
            String dateEndAsString
    ) throws ParseException {
        String taskId = UUID.randomUUID().toString();

        stringValidation(projectId);
        stringValidation(taskName);
        stringValidation(taskDescription);
        stringValidation(dateStartAsString);
        stringValidation(dateEndAsString);

        Date dateStart = convertStringToDate(dateStartAsString);
        Date dateEnd = convertStringToDate(dateEndAsString);

        return taskRepository.persist(taskId, taskName, taskDescription, dateStart, dateEnd, projectId);
    }

    public void updateTask(
            String taskId,
            String name,
            String description,
            String dateStartAsString,
            String dateEndAsString
    ) throws ParseException {
        Date dateStart;
        Date dateEnd;

        stringValidation(taskId);
        Task task = taskRepository.find(taskId);

        if (name.isEmpty()) {
            name = task.getName();
        }

        if (description.isEmpty()) {
            description = task.getDescription();
        }

        if (dateStartAsString.isEmpty()) {
            dateStart = task.getDateStart();
        } else {
            dateStart = convertStringToDate(dateStartAsString);
        }

        if (dateEndAsString.isEmpty()) {
            dateEnd = task.getDateEnd();
        } else {
            dateEnd = convertStringToDate(dateEndAsString);
        }

        taskRepository.merge(task, name, description, dateStart, dateEnd);
    }

    public Map<String, Task> getTasksByProjectId(String projectId) {
        stringValidation(projectId);

        return taskRepository.findAll(projectId);
    }

    public void removeTaskById(String id) {
        stringValidation(id);

        Task removedTask = taskRepository.remove(id);

        if (removedTask == null) {
            throw new IllegalArgumentException("[ЗАДАЧА НЕ НАЙДЕНА]");
        }
    }

    public void removeTasksByProjectId(String projectId) {
        stringValidation(projectId);

        taskRepository.removeALl(projectId);
    }

    private boolean isEmpty(String value) {
        return value == null || value.trim().isEmpty();
    }

    private void stringValidation(String value) {
        if (isEmpty(value)) {
            throw new NullPointerException("[ПОЛЕ НЕ МОЖЕТ БЫТЬ ПУСТЫМ]");
        }
    }

    private Date convertStringToDate(String dateAsString) throws ParseException, IllegalArgumentException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


        return dateFormat.parse(dateAsString);
    }
}
