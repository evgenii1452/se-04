package ru.kozlov.tm.Service;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Repository.ProjectRepository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository){
        this.projectRepository = projectRepository;
    }

    public void removeAllProjects() {
        projectRepository.removeAll();
    }

    public String createProject(
            String name,
            String description,
            String dateStartAsString,
            String dateEndAsString
    ) throws ParseException {
        String id = UUID.randomUUID().toString();

        stringValidation(name);
        stringValidation(description);
        stringValidation(dateStartAsString);
        stringValidation(dateEndAsString);

        Date dateStart = convertStringToDate(dateStartAsString);
        Date dateEnd = convertStringToDate(dateEndAsString);

        return projectRepository.persist(id, name, description, dateStart, dateEnd);
    }

    public String getAllProjectsAsString() {
        StringBuilder projectList = new StringBuilder();
        Map<String, Project> projects = getAllProjects();

        for (Map.Entry<String, Project> projectEntry: projects.entrySet()) {
            projectList.append(projectEntry.getValue().toString()).append("\n");
        }

        return projectList.toString();
    }

    public Map<String, Project> getAllProjects() {
        return projectRepository.findAll();
    }

    public void removeProjectById(String id) {
        stringValidation(id);
        Project project = projectRepository.remove(id);

       if (project == null) {
           throw new IllegalArgumentException("[ПРОЕКТ НЕ НАЙДЕН]");
       }
    }

    public Project getProjectById(String id) {
        stringValidation(id);
        Project project = projectRepository.find(id);

        if (project == null) {
            throw new IllegalArgumentException("[ПРОЕКТ НЕ НАЙДЕН]");
        }

        return project;
    }

    public void updateProject(
            String projectId,
            String name,
            String description,
            String dateStartAsString,
            String dateEndAsString
    ) throws ParseException {
        Date dateStart;
        Date dateEnd;

        stringValidation(projectId);
        Project project = projectRepository.find(projectId);

        if (isEmpty(name)) {
            name = project.getName();
        }

        if (isEmpty(description)) {
            description = project.getDescription();
        }

        if (isEmpty(dateStartAsString)) {
            dateStart = convertStringToDate(dateStartAsString);
        } else {
            dateStart = project.getDateStart();
        }

        if (isEmpty(dateEndAsString)) {
            dateEnd = convertStringToDate(dateEndAsString);
        } else {
            dateEnd = project.getDateEnd();
        }

        projectRepository.merge(project, name, description, dateStart, dateEnd);
    }

    private boolean isEmpty(String value) {
        return value == null || value.trim().isEmpty();
    }

    private void stringValidation(String value) {
        if (isEmpty(value)) {
            throw new NullPointerException("[ПОЛЕ НЕ МОЖЕТ БЫТЬ ПУСТЫМ]");
        }
    }

    private Date convertStringToDate(String dateAsString) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        return dateFormat.parse(dateAsString);
    }
}
