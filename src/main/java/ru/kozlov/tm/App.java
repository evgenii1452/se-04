package ru.kozlov.tm;

import ru.kozlov.tm.Bootstrap.Bootstrap;

public class App {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
