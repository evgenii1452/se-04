package ru.kozlov.tm.Repository;

import ru.kozlov.tm.Entity.Project;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ProjectRepository {
    private Map<String, Project> projects = new HashMap<>();

    public void removeAll() {
        projects.clear();
    }

    public String persist(
            String id,
            String name,
            String description,
            Date dateStart,
            Date dateEnd
    ) {
        Project project = new Project(id ,name, description, dateStart, dateEnd);

        projects.put(id , project);

        return project.getId();
    }

    public Map<String, Project> findAll() {
        return projects;
    }

    public Project remove(String id) {
        return projects.remove(id);
    }

    public Project find(String id) {
        return projects.get(id);
    }

    public void merge(Project project, String name, String description, Date dateStart, Date dateEnd) {
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(dateStart);
        project.setDateEnd(dateEnd);;
    }
}
