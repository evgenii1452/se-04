package ru.kozlov.tm.Repository;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Entity.Task;

import java.util.*;

public class TaskRepository {
    private Map<String, Task> tasks = new HashMap<>();


    public String persist(
            String taskId,
            String taskName,
            String taskDescription,
            Date dateStart,
            Date dateEnd,
            String projectId
    ) {
        Task task = new Task(taskId, taskName, taskDescription, dateStart, dateEnd, projectId);

        tasks.put(taskId, task);

        return task.getId();
    }

    public Map<String, Task> findAll(String projectId) {
        Map<String, Task> projectTasks = new HashMap<>();

        for (Map.Entry<String, Task> task : tasks.entrySet()) {
            if (task.getValue().getProjectId().equals(projectId)) {
                projectTasks.put(task.getKey(), task.getValue());
            }
        }

        return projectTasks;
    }

    public Task remove(String id) {
        return tasks.remove(id);
    }

    public void removeALl(String projectId) {
        Iterator<Map.Entry<String, Task>> it = tasks.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, Task> taskEntry = it.next();

            if (taskEntry.getKey().equals(projectId)) {
                it.remove();
            }
        }
    }

    public Task find(String id) {
        return tasks.get(id);
    }

    public void merge(Task task, String name, String description, Date dateStart, Date dateEnd) {
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(dateStart);
        task.setDateEnd(dateEnd);
    }
}
